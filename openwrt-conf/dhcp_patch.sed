# Copyright (C) 2021 by kitzman

# modify ${PXE_BOOT_PATH}, ${PXE_HOST} and ${PXE_ADDR} accordingly

# enable tftp
s|config dnsmasq$|config dnsmasq\n\toption enable_tftp '1'\n\toption tftp_root '/mnt/tftp'\n|

# add pxe discovery
# failed matching first
$a\\n\# PXE stuff\n
$a\# config match\n\#\toption networkid 'set:bios'\n\#\toption match '60,PXEClient:Arch:00000'\n
$a\# config match\n\#\toption networkid 'set:efi32'\n\#\toption match '60,PXEClient:Arch:00006'\n
$a\# config match\n\#\toption networkid 'set:efi64'\n\#\toption match '60,PXEClient:Arch:00007'\n
$a\# config match\n\#\toption networkid 'set:efi64'\n\#\toption match '60,PXEClient:Arch:00009'\n

# boot config for bios + efi32 (commented)
$a\# THIS NEEDS REWRITING
$a\#config boot\n\#\toption filename\t'tag:bios,lpxelinux.0'
$a\#\toption serveraddress\t'${PXE_ADDR}'
$a\#\toption servername\t'${PXE_HOST}'\n

$a\# THIS NEEDS REWRITING
$a\#config boot\n\#\toption filename\t'tag:efi32,core.efi'
$a\#\toption serveraddress\t'${PXE_ADDR}'
$a\#\toption servername\t'${PXE_HOST}'\n

# and working efi64
$aconfig boot\n\toption filename\t'${PXE_BOOT_PATH}/grub/x86_64-efi/core.efi'
$a\\toption serveraddress\t'${PXE_ADDR}'
$a\\toption servername\t'${PXE_HOST}'
