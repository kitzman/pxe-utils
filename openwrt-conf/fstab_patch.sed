# Copyright (C) 2021 by kitzman

# /dev/sda1 configuration - adjust to your needs
# by replacing ${MOUNTPOINT} and ${DEVICE}
$a\\n\#fstab auto-mount
$a\\nconfig mount
$a\\toption target\t${MOUNTPOINT}
$a\\toption device\t${DEVICE}
$a\\toption enabled\t1
$a\\toption enabled_fsck\t0\n
