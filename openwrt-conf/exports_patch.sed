# Copyright (C) 2021 by kitzman

# modify ${NFS_ROOT} accordingly

$a\\n\# NFS mounts
$a${NFS_ROOT} *(rw,sync,no_subtree_check,no_root_squash,fsid=0)\n
