#!/bin/sh

# log function
function log() {
    logger -p daemon.notice -t pxe_watcher $@
}

if [ -z "$1" ] || [ -z "$2" ]; then
    log "usage: ./pxe_watcher <mountpoint> <pid path>"
    exit 1
fi

nfsd_path=/etc/init.d/nfsd
wait_amount=5s
pid_file=$2

max_failures=5
max_stops=3

watchpoint=$1

if ! [ -f $nfsd_path ]; then
    log "could not find nfsd daemon! exiting"
    exit 1
fi

# Making sure the service is stopped & disabled

$nfsd_path disable || true
$nfsd_path stop || true

# watchpoint function
function watch_mountpoint() {
    failure_count=0
    started=0
    while true; do
	if [ $started == 0 ] && [ -d $watchpoint ]; then
	    log "starting $nfsd_path for $watchpoint"
	    $nfsd_path start
	elif [ $started == 1 ] && [ ! -d $watchpoint ]; then
	    log "stopping $nfsd_path for $watchpoint"
	    $nfsd_path stop
	else
	    sleep $wait_amount
	    continue
	fi
	
	if [ $? != 0 ]; then
	    failure_count=$(($failure_count + 1))
	else
	    started=$(($started ^ 1))
	    failure_count=0
	fi

	if [ $failure_count == $max_failures ]; then
	    log "$max_failures failures reached! quitting"
	    return 1
	fi
	sleep $wait_amount
    done
}

# interrupt function
function sigint_handler() {
    stops=0
    did_stop=0

    while [ ! $stops == $max_stops ]; do
	log "stopping $nfsd_path ($stops)"
	$nfsd_path stop
	if [ $? == 0 ]; then
	    did_stop=1
	    break
        fi
	stops=$(($stops + 1))
    done

    rm $pid_file || true
    
    if [ $did_stop == 0 ]; then
	log "could not stop $nfsd_path!"
	exit 1
    fi
}

log "watching $watchpoint ..."
trap sigint_handler EXIT

echo $$ > $pid_file

watch_mountpoint

log "exiting"
